package config

import (
	"io/ioutil"

	"github.com/pkg/errors"
	"gopkg.in/yaml.v2"
)

type Options struct {
	Transmission TransmissionOptions `yaml:"transmission"`
	Telegram     TelegramOptions     `yaml:"telegram"`
	Users        UsersOptions        `yaml:"users"`
	DB           DBOptions           `yaml:"db"`
	Paths        PathOptions         `yaml:"paths"`
}

type TransmissionOptions struct {
	URL string `yaml:"url"`
}

type TelegramOptions struct {
	Token string `yaml:"token"`
}

type UsersOptions struct {
	Admins       []string `yaml:"admins"`
	AllowedUsers []string `yaml:"allowed-users"`
}

type DBOptions struct {
	Filename string `yaml:"filename"`
}

type PathOptions struct {
	SharePath string `yaml:"share-path"`
	LogPath   string `yaml:"log-path"`
}

func LoadConfig(filePath string) (config Options, err error) {
	rawConfig, err := ioutil.ReadFile(filePath)
	if err != nil {
		err = errors.WithStack(err)
		return
	}
	err = yaml.Unmarshal(rawConfig, &config)
	if err != nil {
		err = errors.WithStack(err)
		return
	}
	return
}
