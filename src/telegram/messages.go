package telegram

import (
	"encoding/json"
	"strconv"
)

type ToMapStringByteser interface {
	ToMapStringBytes() map[string][]byte
}

type Message struct {
	MessageID            int                  `json:"message_id"`
	From                 User                 `json:"from"`
	Date                 int                  `json:"date"`
	Chat                 Chat                 `json:"chat"`
	ForwardFrom          User                 `json:"forward_from"`
	ForwardFromChat      Chat                 `json:"forward_from_chat"`
	ForwardFromMessageID int                  `json:"forward_from_message_id"`
	ForwardSignature     string               `json:"forward_signature"`
	ForwardSenderName    string               `json:"forward_sender_name"`
	ForwardDate          int                  `json:"forward_date"`
	EditDate             int                  `json:"edit_date"`
	MediaGroupID         string               `json:"media_group_id"`
	AuthorSignature      string               `json:"author_signature"`
	Text                 string               `json:"text"`
	Document             Document             `json:"document"`
	ReplyMarkup          InlineKeyboardMarkup `json:"reply_markup"`
}

type MessageSenderI interface {
	Send(telC *Client) ([]byte, error)
}

type EditMessageReplyMarkup struct {
	ChatID          int                  `json:"chat_id,omitempty"`
	MessageID       int                  `json:"message_id,omitempty"`
	InlineMessageID string               `json:"inline_message_id,omitempty"`
	ReplyMarkup     InlineKeyboardMarkup `json:"reply_markup,omitempty"`
}

func (emrm EditMessageReplyMarkup) Send(client *Client) ([]byte, error) {
	return client.SendEditMessageReplyMarkup(emrm)
}

type EditMessageText struct {
	ChatID                int                  `json:"chat_id,omitempty"`
	MessageID             int                  `json:"message_id,omitempty"`
	InlineMessageID       string               `json:"inline_message_id,omitempty"`
	Text                  string               `json:"text"`
	ParseMode             string               `json:"parse_mode,omitempty"`
	DisableWebPagePreview bool                 `json:"disable_web_page_preview,omitempty"`
	ReplyMarkup           InlineKeyboardMarkup `json:"reply_markup,omitempty"`
}

func (emt EditMessageText) Send(client *Client) ([]byte, error) {
	return client.SendEditMessageText(emt)
}

type OutgoingMessage struct {
	ChatID                int                  `json:"chat_id"`
	Text                  string               `json:"text"`
	ParseMode             string               `json:"parse_mode,omitempty"`
	DisableWebPagePreview bool                 `json:"disable_web_page_preview,omitempty"`
	DisableNotification   bool                 `json:"disable_notification,omitempty"`
	ReplyToMessageID      int                  `json:"reply_to_message_id,omitempty"`
	ReplyMarkup           InlineKeyboardMarkup `json:"reply_markup,omitempty"`
}

func (om OutgoingMessage) Send(client *Client) ([]byte, error) {
	return client.SendMessage(om)
}

type AnswerCallbackQuery struct {
	CallbackQueryID string `json:"callback_query_id"`
	Text            string `json:"text,omitempty"`
	ShowAlert       bool   `json:"show_alert,omitempty"`
	URL             string `json:"url,omitempty"`
	CacheTime       int    `json:"cache_time,omitempty"`
}

func (acq AnswerCallbackQuery) Send(client *Client) ([]byte, error) {
	return client.SendCallbackAnswer(acq)
}

type SendDocument struct {
	ChatID              int                  `json:"chat_id"`
	Document            string               `json:"document,omitempty"`
	Thumb               string               `json:"thumb,omitempty"`
	Caption             string               `json:"caption,omitempty"`
	ParseMode           string               `json:"parse_mode,omitempty"`
	DisableNotification string               `json:"disable_notification,omitempty"`
	ReplyToMessageID    int                  `json:"reply_to_message_id,omitempty"`
	ReplyMarkup         InlineKeyboardMarkup `json:"reply_markup,omitempty"`
	FileName            string
}

func (sd SendDocument) ToMapStringBytes() map[string][]byte {
	result := make(map[string][]byte)
	result["chat_id"] = []byte(strconv.Itoa(sd.ChatID))
	if sd.Document != "" {
		result["document"] = []byte(`"` + sd.Document + `"`)
	}
	if sd.Thumb != "" {
		result["thumb"] = []byte(`"` + sd.Thumb + `"`)
	}
	if sd.Caption != "" {
		result["caption"] = []byte(`"` + sd.Caption + `"`)
	}
	if sd.ParseMode != "" {
		result["parse_mode"] = []byte(`"` + sd.ParseMode + `"`)
	}
	if sd.DisableNotification != "" {
		result["disable_notification"] = []byte(`"` + sd.DisableNotification + `"`)
	}
	if sd.ReplyToMessageID != 0 {
		result["reply_to_message_id"] = []byte(strconv.Itoa(sd.ReplyToMessageID))
	}
	if !sd.ReplyMarkup.IsZero() {
		result["reply_markup"], _ = json.Marshal(sd.ReplyMarkup)
	}
	return result
}

func (sd SendDocument) Send(client *Client) ([]byte, error) {
	return client.SendDocument(sd)
}
