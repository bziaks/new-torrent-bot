package telegram

type Chat struct {
	ID       int    `json:"id"`
	Username string `json:"username"`
}

type Document struct {
	Filename string `json:"file_name"`
	FileID   string `json:"file_id"`
}

type UpdateResult struct {
	UpdateID      int           `json:"update_id"`
	Message       Message       `json:"message"`
	EditedMessage Message       `json:"edited_message"`
	CallbackQuery CallbackQuery `json:"callback_query"`
}

type Update struct {
	Result []UpdateResult `json:"result"`
}

type User struct {
	ID           int    `json:"id"`
	IsBot        bool   `json:"is_bot"`
	FirstName    string `json:"first_name"`
	LastName     string `json:"last_name"`
	Username     string `json:"username"`
	LanguageCode string `json:"language_code"`
}

type CallbackQuery struct {
	ID              string  `json:"id"`
	From            User    `json:"from"`
	Message         Message `json:"message"`
	InlineMessageID string  `json:"inline_message_id"`
	ChatInstance    string  `json:"chat_instance"`
	Data            string  `json:"data"`
}

type InlineKeyboardButton struct {
	Text                         string `json:"text"`
	URL                          string `json:"url,omitempty"`
	LoginURL                     string `json:"login_url,omitempty"`
	CallbackData                 string `json:"callback_data,omitempty"`
	SwitchInlineQuery            string `json:"switch_inline_query,omitempty"`
	SwitchInlineQueryCurrentChat string `json:"switch_inline_query_current_chat,omitempty"`
	Pay                          bool   `json:"pay,omitempty"`
}

type InlineKeyboardMarkup struct {
	InlineKeyboard [][]InlineKeyboardButton `json:"inline_keyboard,omitempty"`
}

func (ikm *InlineKeyboardMarkup) AddKey(key InlineKeyboardButton) {
	ikm.InlineKeyboard = append(ikm.InlineKeyboard, []InlineKeyboardButton{key})
}

func (ikm *InlineKeyboardMarkup) IsZero() bool {
	if ikm.InlineKeyboard == nil {
		return true
	}
	return false
}
