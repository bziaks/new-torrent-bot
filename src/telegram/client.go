package telegram

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"os"
	"time"
)

const (
	telegramAPIBotURLTemplate    = "https://api.telegram.org/bot%s/%s"
	telegramFileDownloadTemplate = "https://api.telegram.org/file/bot%s/%s"

	sendMessageMethod            = "sendMessage"
	getUpdatesMethod             = "getUpdates"
	getFileMethod                = "getFile"
	answerCallbackQueryMethod    = "answerCallbackQuery"
	editMessageReplyMarkupMethod = "editMessageReplyMarkup"
	editMessageTextMethod        = "editMessageText"
	sendDocumentMethod           = "sendDocument"

	fileParameter = "document"
)

func GetRequestOverProxy(url string, client http.Client) (respBody []byte, err error) {
	resp, err := client.Get(url)
	if err != nil {
		return
	}
	respBody, err = ioutil.ReadAll(resp.Body)
	if resp.StatusCode != 200 {
		err = fmt.Errorf("incorrect file download: %s", string(respBody))
	}
	return
}

type Client struct {
	tgURL      string
	token      string
	httpClient http.Client
}

func New(token string) *Client {
	return &Client{
		tgURL:      telegramAPIBotURLTemplate,
		token:      token,
		httpClient: http.Client{Timeout: 25 * time.Second},
	}
}

func (telC Client) makeRequest(HTTPMethod, method string, data interface{}) (respBody []byte, err error) {
	url := fmt.Sprintf(telC.tgURL, telC.token, method)
	reqBody, err := json.Marshal(data)
	b := bytes.NewReader(reqBody)
	req, err := http.NewRequest(HTTPMethod, url, b)
	if err != nil {
		return
	}
	req.Header.Set("Content-Type", "application/json")
	resp, err := telC.httpClient.Do(req)
	if err != nil {
		return
	}
	defer resp.Body.Close()
	if resp.StatusCode == 200 {
		respBody, err = ioutil.ReadAll(resp.Body)
	} else {
		respBody, _ := ioutil.ReadAll(resp.Body)
		err = errors.New(string(respBody))
	}
	return
}

func (telC Client) makeRequestWithFile(HTTPMethod, method string, data ToMapStringByteser, fileParam, fileName string) (respBody []byte, err error) {
	url := fmt.Sprintf(telC.tgURL, telC.token, method)

	file, err := os.Open(fileName)
	if err != nil {
		return
	}
	defer file.Close()

	fileInfo, err := file.Stat()
	if err != nil {
		return
	}

	body := new(bytes.Buffer)
	writer := multipart.NewWriter(body)

	for k, v := range data.ToMapStringBytes() {
		errS := writer.WriteField(k, string(v))
		if errS != nil {
			err = errS
			return
		}
	}

	part, err := writer.CreateFormFile(fileParam, fileInfo.Name())
	if err != nil {
		return
	}
	_, err = io.Copy(part, file)
	if err != nil {
		return
	}

	if err = writer.Close(); err != nil {
		return
	}

	req, err := http.NewRequest(HTTPMethod, url, body)
	if err != nil {
		return
	}
	req.Header.Add("Content-Type", writer.FormDataContentType())
	req.ContentLength = int64(body.Len())

	resp, err := telC.httpClient.Do(req)
	if err != nil {
		return
	}
	defer resp.Body.Close()
	if resp.StatusCode == 200 {
		respBody, err = ioutil.ReadAll(resp.Body)
	} else {
		respBody, _ := ioutil.ReadAll(resp.Body)
		err = errors.New(string(respBody))
	}
	return
}

func (telC Client) SendMessage(message OutgoingMessage) ([]byte, error) {
	return telC.makeRequest(http.MethodPost, sendMessageMethod, message)
}

func (telC Client) SendEditMessageReplyMarkup(message EditMessageReplyMarkup) ([]byte, error) {
	return telC.makeRequest(http.MethodPost, editMessageReplyMarkupMethod, message)
}

func (telC Client) SendEditMessageText(message EditMessageText) ([]byte, error) {
	return telC.makeRequest(http.MethodPost, editMessageTextMethod, message)
}

func (telC Client) SendDocument(message SendDocument) ([]byte, error) {
	return telC.makeRequestWithFile(http.MethodPost, sendDocumentMethod, message, fileParameter, message.FileName)
}

func (telC Client) GetUpdates(offset int) (result Update, err error) {
	data := struct {
		Offset int `json:"offset"`
	}{offset + 1}
	resp, err := telC.makeRequest(http.MethodPost, getUpdatesMethod, data)
	if err != nil {
		return
	}
	err = json.Unmarshal(resp, &result)
	return
}

func (telC Client) SendCallbackAnswer(callbackAnswer AnswerCallbackQuery) ([]byte, error) {
	return telC.makeRequest(http.MethodPost, answerCallbackQueryMethod, callbackAnswer)
}

func (telC Client) GetFile(fileID string) (file []byte, err error) {
	data := struct {
		FileID string `json:"file_id"`
	}{fileID}
	resp, err := telC.makeRequest(http.MethodPost, getFileMethod, data)
	if err != nil {
		return
	}
	filePath := struct {
		Result struct {
			FilePath string `json:"file_path"`
		} `json:"result"`
	}{}
	err = json.Unmarshal(resp, &filePath)
	if err != nil {
		return
	}
	fileURL := fmt.Sprintf(telegramFileDownloadTemplate, telC.token, filePath.Result.FilePath)
	file, err = GetRequestOverProxy(fileURL, telC.httpClient)
	return
}
