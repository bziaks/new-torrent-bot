package workers

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"sync"

	"github.com/pkg/errors"
	"gitlab.com/bziaks/new-torrent-bot/src/db"
	"gitlab.com/bziaks/new-torrent-bot/src/telegram"
	"gitlab.com/bziaks/new-torrent-bot/src/transmission"
)

const (
	TorrentMessageTemplate = "Торрент %s загружен"
	TorrentDonePath        = "/torrent-done"
)

type Endpoints struct {
	dbase                *db.DBLite
	outgoingMessagesChan chan telegram.MessageSenderI
	errorsChan           chan error
	TransmissionClient   *transmission.Client
}

func (e *Endpoints) TorrentDoneHandler(w http.ResponseWriter, req *http.Request) {
	reqBody, err := ioutil.ReadAll(req.Body)
	if err != nil {
		e.errorsChan <- errors.WithStack(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	var torrent transmission.Torrent
	err = json.Unmarshal(reqBody, &torrent)
	if err != nil {
		e.errorsChan <- errors.WithStack(err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	go func() {
		_, err = e.TransmissionClient.RemoveTorrent([]int{torrent.ID})
		if err != nil {
			e.errorsChan <- errors.WithStack(err)
		}
	}()
	chatID, err := e.dbase.GetChatByTorrentID(torrent.ID)
	if err != nil {
		e.errorsChan <- errors.WithStack(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	message := telegram.OutgoingMessage{
		ChatID: chatID,
		Text:   fmt.Sprintf(TorrentMessageTemplate, torrent.Name),
	}
	e.outgoingMessagesChan <- message
	err = e.dbase.DeleteTorrent(torrent.ID)
	if err != nil {
		e.errorsChan <- errors.WithStack(err)
	}
	w.WriteHeader(http.StatusAccepted)
}

func (e *Endpoints) Run(wg *sync.WaitGroup) {
	defer wg.Done()
	http.HandleFunc(TorrentDonePath, e.TorrentDoneHandler)
	log.Println(http.ListenAndServe(":8090", nil))
}
