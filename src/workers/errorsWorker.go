package workers

import (
	"log"
	"sync"
)

type ErrorsWorker struct {
	errorsChan <-chan error
}

func (ew *ErrorsWorker) Run(wg *sync.WaitGroup) {
	defer wg.Done()
	for err := range ew.errorsChan {
		log.Printf("%+v", err)
	}
}
