package workers

import (
	"sync"
	"time"

	"github.com/pkg/errors"
	"gitlab.com/bziaks/new-torrent-bot/src/db"
	"gitlab.com/bziaks/new-torrent-bot/src/telegram"
	"gitlab.com/bziaks/new-torrent-bot/src/transmission"
)

const (
	TelegramWatcherPeriod = 1000 * time.Millisecond
	TorrentWatcherPeriod  = 2 * time.Minute
)

type Watcher struct {
	incomingMessagesChan chan telegram.UpdateResult
	outgoingMessagesChan chan telegram.MessageSenderI
	errorsChan           chan error
	TransmissionClient   *transmission.Client
	TelegramClient       *telegram.Client
	dbase                *db.DBLite
}

func (w *Watcher) watchTorrents() {
	torrentsResponse, err := w.TransmissionClient.GetStatusAll()
	if err != nil {
		w.errorsChan <- errors.WithStack(err)
		return
	}
	if len(torrentsResponse.Arguments.Torrents) == 0 {
		err = w.dbase.DeleteAllTorrents()
		if err != nil {
			w.errorsChan <- errors.WithStack(err)
		}
	}
}

func (w *Watcher) watchTGMessages() {
	offset, err := w.dbase.GetLastUpdate()
	if err != nil {
		w.errorsChan <- errors.WithStack(err)
		return
	}
	res, err := w.TelegramClient.GetUpdates(offset)
	if err != nil {
		w.errorsChan <- errors.WithStack(err)
		return
	}
	for _, message := range res.Result {
		w.incomingMessagesChan <- message
		w.dbase.UpdateLastUpdateID(message.UpdateID)
	}
}

func (w *Watcher) Run(wg *sync.WaitGroup) {
	defer wg.Done()
	telegramTicker := time.NewTicker(TelegramWatcherPeriod)
	torrentTicker := time.NewTicker(TorrentWatcherPeriod)
	go func() {
		for range telegramTicker.C {
			w.watchTGMessages()
		}
	}()
	go func() {
		for range torrentTicker.C {
			w.watchTorrents()
		}
	}()
}
