package workers

import (
	"sync"

	"github.com/pkg/errors"
	"gitlab.com/bziaks/new-torrent-bot/src/commands"
	"gitlab.com/bziaks/new-torrent-bot/src/telegram"
	"gitlab.com/bziaks/new-torrent-bot/src/transmission"
)

func CheckAllowedUsers(Username string) bool {
	var AllowedUsers = [2]string{"bziaks", "natasha_fp"}
	for _, el := range AllowedUsers {
		if el == Username {
			return true
		}
	}
	return false
}

type IncomingTelegramWorker struct {
	torrentChan          chan torrent
	incomingMessagesChan <-chan telegram.UpdateResult
	outgoingMessagesChan chan telegram.MessageSenderI
	errorsChan           chan error
	TransmissionClient   *transmission.Client
	TelegramClient       *telegram.Client
	TelegramCommands     commands.Commands
	TelegramCallbacks    commands.Callbacks
}

func (itw *IncomingTelegramWorker) Run(wg *sync.WaitGroup) {
	defer wg.Done()
	for update := range itw.incomingMessagesChan {
		if !CheckAllowedUsers(update.Message.Chat.Username) && !CheckAllowedUsers(update.CallbackQuery.From.Username) {
			itw.outgoingMessagesChan <- telegram.OutgoingMessage{ChatID: update.Message.Chat.ID, Text: "Неправильный отправитель"}
			continue
		}
		if update.CallbackQuery.Data != "" {
			go func() {
				response, err := itw.TelegramCallbacks.AnswerCallback(update.CallbackQuery)
				if err != nil {
					itw.errorsChan <- errors.WithStack(err)
					return
				}
				itw.outgoingMessagesChan <- response
			}()
			continue
		}
		if update.Message.Text != "" && update.Message.Text[:1] == "/" {
			go func() {
				response, err := itw.TelegramCommands.HandleMessage(update.Message)
				if err != nil {
					itw.errorsChan <- errors.WithStack(err)
					return
				}
				itw.outgoingMessagesChan <- response
			}()
			continue
		}
		if update.Message.Document.FileID == "" && update.Message.Document.Filename == "" {
			itw.outgoingMessagesChan <- telegram.OutgoingMessage{ChatID: update.Message.Chat.ID, Text: "Неизвестная команда"}
			continue
		}
		chatID := update.Message.Chat.ID
		fileID := update.Message.Document.FileID
		go func() {
			FileForDownload, err := itw.TelegramClient.GetFile(fileID)
			if err != nil {
				itw.errorsChan <- errors.WithStack(err)
				return
			}
			itw.torrentChan <- torrent{chatID: chatID, body: FileForDownload}
		}()
	}
}

type OutgoingTelegramWorker struct {
	outgoingMessagesChan <-chan telegram.MessageSenderI
	errorsChan           chan error
	TelegramClient       *telegram.Client
}

func (otw *OutgoingTelegramWorker) Run(wg *sync.WaitGroup) {
	defer wg.Done()
	for message := range otw.outgoingMessagesChan {
		_, err := message.Send(otw.TelegramClient)
		if err != nil {
			otw.errorsChan <- errors.WithStack(err)
		}
	}
}
