package workers

import (
	"sync"

	"gitlab.com/bziaks/new-torrent-bot/src/commands"
	"gitlab.com/bziaks/new-torrent-bot/src/config"
	"gitlab.com/bziaks/new-torrent-bot/src/db"
	"gitlab.com/bziaks/new-torrent-bot/src/telegram"
	"gitlab.com/bziaks/new-torrent-bot/src/transmission"
)

const (
	selectTorrentCallback = "select-torrent"
	deleteTorrentCallback = "delete-torrent"
	stopTorrentCallback   = "stop-torrent"
	resumeTorrentCallback = "resume-torrent"
	backCallback          = "back-to-torrent-list"
	updateCallback        = "update-torrents-list"
	selectFileCallback    = "select-file"
	deleteFileCallback    = "delete-file"
	downloadFileCallback  = "download-file"

	statusCommand     = "/status"
	freeSpaceCommand  = "/freespace"
	usedMemoryCommand = "/usedmemory"
	logsCommand       = "/logs"
	filesCommand      = "/files"
)

type WorkerInterface interface {
	Run(wg *sync.WaitGroup)
}

func initCommands(handlers commands.Handlers) commands.Commands {
	c := commands.NewCommands()
	c.Add(statusCommand, handlers.StatusCommandHandler)
	c.Add(freeSpaceCommand, handlers.FreeSpaceCommandHandler)
	c.Add(usedMemoryCommand, handlers.UsedMemoryCommandHandler)
	c.Add(logsCommand, handlers.LogCommandHandler)
	c.Add(filesCommand, handlers.FilesCommandHandler)
	return c
}

func initCallbacks(handlers commands.Handlers) commands.Callbacks {
	c := commands.NewCallbacks()
	c.Add(selectTorrentCallback, handlers.SelectTorrentCallbackHandler)
	c.Add(deleteTorrentCallback, handlers.RemoveTorrentCallbackHandler)
	c.Add(stopTorrentCallback, handlers.StopTorrentCallbackHandler)
	c.Add(resumeTorrentCallback, handlers.ResumeTorrentCallbackHandler)
	c.Add(backCallback, handlers.UpdateTorrentsListCallbackHandler)
	c.Add(selectFileCallback, handlers.SelectFileCallbackHandler)
	c.Add(deleteFileCallback, handlers.RemoveFileCallbackHandler)
	c.Add(updateCallback, handlers.UpdateTorrentsListCallbackHandler)
	c.Add(downloadFileCallback, handlers.DownloadFileCallbackHandler)
	return c
}

func NewWorkers(options config.Options, dbase *db.DBLite) []WorkerInterface {
	telClient := telegram.New(options.Telegram.Token)
	transClient := transmission.New(options.Transmission.URL)
	errorsChan := make(chan error, 10)
	incomingChan := make(chan telegram.UpdateResult, 100)
	outgoingChan := make(chan telegram.MessageSenderI, 100)
	torrentsChan := make(chan torrent, 20)
	handlers := commands.NewHandlers(transClient, outgoingChan, dbase, options)
	telegramCallbacks := initCallbacks(handlers)
	telegramCommands := initCommands(handlers)
	itw := &IncomingTelegramWorker{
		torrentChan:          torrentsChan,
		incomingMessagesChan: incomingChan,
		outgoingMessagesChan: outgoingChan,
		errorsChan:           errorsChan,
		TransmissionClient:   transClient,
		TelegramClient:       telClient,
		TelegramCommands:     telegramCommands,
		TelegramCallbacks:    telegramCallbacks,
	}
	otw := &OutgoingTelegramWorker{
		outgoingMessagesChan: outgoingChan,
		errorsChan:           errorsChan,
		TelegramClient:       telClient,
	}
	tw := &TorrentWorker{
		errorsChan:           errorsChan,
		outgoingMessagesChan: outgoingChan,
		torrentChan:          torrentsChan,
		dbase:                dbase,
		transmissionClient:   transClient,
	}
	watcher := &Watcher{
		outgoingMessagesChan: outgoingChan,
		incomingMessagesChan: incomingChan,
		errorsChan:           errorsChan,
		TransmissionClient:   transClient,
		TelegramClient:       telClient,
		dbase:                dbase,
	}
	webEndpoints := &Endpoints{
		TransmissionClient:   transClient,
		dbase:                dbase,
		outgoingMessagesChan: outgoingChan,
		errorsChan:           errorsChan,
	}
	ew := &ErrorsWorker{errorsChan: errorsChan}
	return []WorkerInterface{itw, otw, tw, watcher, ew, webEndpoints}
}
