package workers

import (
	"fmt"
	"sync"

	"github.com/pkg/errors"
	"gitlab.com/bziaks/new-torrent-bot/src/db"
	"gitlab.com/bziaks/new-torrent-bot/src/telegram"
	"gitlab.com/bziaks/new-torrent-bot/src/transmission"
)

const (
	torrentDownloadingTemplate        = "Торрент \"%s\" добавлен для скачивания"
	torrentAlreadyDownloadingTemplate = "Торрент \"%s\" уже скачивается"

	torrentUnknownError = "Ошибка при добавлении торрента"
)

type torrent struct {
	chatID int
	body   []byte
}

type TorrentWorker struct {
	torrentChan          <-chan torrent
	outgoingMessagesChan chan telegram.MessageSenderI
	errorsChan           chan error
	transmissionClient   *transmission.Client
	dbase                *db.DBLite
}

func (tw *TorrentWorker) sendResponse(chatID int, text string) {
	StatusMessage := telegram.OutgoingMessage{ChatID: chatID, Text: text}
	tw.outgoingMessagesChan <- StatusMessage
}

func (tw *TorrentWorker) Run(wg *sync.WaitGroup) {
	defer wg.Done()
	for t := range tw.torrentChan {
		trans, err := tw.transmissionClient.SetTorrentToDownload(t.body)
		if err != nil {
			tw.errorsChan <- errors.WithStack(err)
		}
		if trans.Arguments.TorrentAdded.ID != 0 && trans.Arguments.TorrentAdded.Name != "" {
			torrentID := trans.Arguments.TorrentAdded.ID
			tw.sendResponse(t.chatID, fmt.Sprintf(torrentDownloadingTemplate, trans.Arguments.TorrentAdded.Name))
			err = tw.dbase.AddTorrent(t.chatID, torrentID)
			if err != nil {
				tw.errorsChan <- errors.WithStack(err)
			}
		} else {
			if trans.Arguments.TorrentDuplicate.ID != 0 {
				tw.sendResponse(t.chatID, fmt.Sprintf(torrentAlreadyDownloadingTemplate, trans.Arguments.TorrentDuplicate.Name))
			} else {
				tw.sendResponse(t.chatID, torrentUnknownError)
			}
		}

	}
}
