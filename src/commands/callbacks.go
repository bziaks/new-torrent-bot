package commands

import (
	"fmt"
	"strings"

	"gitlab.com/bziaks/new-torrent-bot/src/telegram"
)

type Callbacks map[string]func(callback telegram.CallbackQuery) (Result telegram.AnswerCallbackQuery, err error)

func NewCallbacks() Callbacks {
	return Callbacks{}
}

func (c Callbacks) Add(callback string, handler func(callback telegram.CallbackQuery) (Result telegram.AnswerCallbackQuery, err error)) {
	c[callback] = handler
}

func (c Callbacks) Get(callback string) (handler func(callback telegram.CallbackQuery) (Result telegram.AnswerCallbackQuery, err error), ok bool) {
	handler, ok = c[callback]
	return
}

func (c Callbacks) AnswerCallback(callbackQuery telegram.CallbackQuery) (Result telegram.AnswerCallbackQuery, err error) {
	parsedText := strings.Split(callbackQuery.Data, " ")
	callback := parsedText[0]
	handler, ok := c[callback]
	if !ok {
		err = fmt.Errorf("can't find callback")
		return
	}
	Result, err = handler(callbackQuery)
	Result.CallbackQueryID = callbackQuery.ID
	return
}
