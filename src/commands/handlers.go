package commands

import (
	"bufio"
	"fmt"
	"os"
	"path/filepath"
	"runtime"
	"strconv"
	"strings"

	"gitlab.com/bziaks/new-torrent-bot/src/config"

	"github.com/pkg/errors"
	"gitlab.com/bziaks/new-torrent-bot/src/db"
	"gitlab.com/bziaks/new-torrent-bot/src/telegram"
	"gitlab.com/bziaks/new-torrent-bot/src/transmission"
	"gitlab.com/bziaks/new-torrent-bot/src/utils"
	"golang.org/x/sys/unix"
)

const (
	StatusTemplate = `Имя: %s
Процентов скачано: %.3f%%
Скорость скачивания: %s/s
Общий размер: %s
`
	FreeSpaceTemplate  = `Свободное место на диске: %s`
	UsedMemoryTemplate = `Использовано памяти: Sys - %s, Alloc - %s, Total alloc - %s`

	MarkdownParseMode = "Markdown"

	torrentStatusStopped = 0 //https://github.com/transmission/transmission/blob/2.94/libtransmission/transmission.h#L1815

	resumeButtonText   = "Возобновить"
	stopButtonText     = "Остановить"
	removeButtonText   = "Удалить"
	backButtonText     = "Назад"
	upButtonText       = "Вверх"
	updateButtonText   = "Обновить"
	downloadButtonText = "Скачать"

	selectTorrentDataTemplate = "select-torrent %d"
	resumeTorrentDataTemplate = "resume-torrent %d"
	stopTorrentDataTemplate   = "stop-torrent %d"
	removeTorrentDataTemplate = "delete-torrent %d"
	backData                  = "back-to-torrent-list"
	updateData                = "update-torrents-list"

	selectFileDataTemplate = "select-file %d"
	deleteFileDataTemplate = "delete-file %d"
	downloadFileData       = "download-file"

	maxFileSizeForDownload = 52_428_800 // 50 megabytes
)

func PrepareFilesListWithKeyboard(file string) (filesListText string, keyboard telegram.InlineKeyboardMarkup, err error) {
	fileInfo, err := os.Stat(file)
	if err != nil {
		return
	}
	builder := strings.Builder{}
	builder.WriteString("```\n")
	builder.WriteString(file)
	builder.WriteString("\n\n")
	key := telegram.InlineKeyboardButton{
		Text:         removeButtonText,
		CallbackData: fmt.Sprintf(deleteFileDataTemplate, -1),
	}
	keyboard.AddKey(key)
	key = telegram.InlineKeyboardButton{
		Text:         upButtonText,
		CallbackData: fmt.Sprintf(selectFileDataTemplate, -1),
	}
	keyboard.AddKey(key)
	if !fileInfo.IsDir() {
		if fileInfo.Size() < maxFileSizeForDownload {
			key = telegram.InlineKeyboardButton{
				Text:         downloadButtonText,
				CallbackData: downloadFileData,
			}
			keyboard.AddKey(key)
		}
		builder.WriteString("```")
		filesListText = builder.String()
		return
	}
	newFiles, err := utils.ListDirectory(file)
	if err != nil {
		return
	}
	for i, newFile := range newFiles {
		builder.WriteString(newFile)
		builder.WriteString("\n")
		key := telegram.InlineKeyboardButton{
			Text:         newFile,
			CallbackData: fmt.Sprintf(selectFileDataTemplate, i),
		}
		keyboard.AddKey(key)
	}
	builder.WriteString("```")
	filesListText = builder.String()
	return
}

type Handlers struct {
	transmissionClient   *transmission.Client
	outgoingMessagesChan chan telegram.MessageSenderI
	dbase                *db.DBLite
	options              config.Options
}

func NewHandlers(transmissionClient *transmission.Client, outgoingMessagesChan chan telegram.MessageSenderI, dbase *db.DBLite, options config.Options) Handlers {
	return Handlers{
		transmissionClient:   transmissionClient,
		outgoingMessagesChan: outgoingMessagesChan,
		dbase:                dbase,
		options:              options,
	}
}

func (h Handlers) StatusCommandHandler(args string) (Result telegram.OutgoingMessage, err error) {
	text, keyboard, err := h.prepareStatusMessage()
	if err != nil {
		return
	}
	Result.ReplyMarkup = keyboard
	Result.Text = text
	return
}

func (h Handlers) FreeSpaceCommandHandler(args string) (Result telegram.OutgoingMessage, err error) {
	var stat unix.Statfs_t
	err = unix.Statfs(h.options.Paths.SharePath, &stat)
	if err != nil {
		err = errors.WithStack(err)
		return
	}
	free := uint64(stat.Bsize) * stat.Bavail
	humanReadableFree := utils.BytesCount(int64(free))
	Result.Text = fmt.Sprintf(FreeSpaceTemplate, humanReadableFree)
	return
}

func (h Handlers) UsedMemoryCommandHandler(args string) (Result telegram.OutgoingMessage, err error) {
	var mem runtime.MemStats
	runtime.ReadMemStats(&mem)
	humanReadableSysMem := utils.BytesCount(int64(mem.Sys))
	humanReadableAllocMem := utils.BytesCount(int64(mem.Alloc))
	humanReadableTotalAllocMem := utils.BytesCount(int64(mem.TotalAlloc))
	Result.Text = fmt.Sprintf(UsedMemoryTemplate, humanReadableSysMem, humanReadableAllocMem, humanReadableTotalAllocMem)
	return
}

func (h Handlers) LogCommandHandler(args string) (Result telegram.OutgoingMessage, err error) {
	Result.ParseMode = MarkdownParseMode
	args = strings.TrimSpace(args)
	linesCount, err := strconv.Atoi(args)
	if err != nil {
		return
	}
	logFile, err := os.Open(h.options.Paths.LogPath)
	if err != nil {
		return
	}
	defer logFile.Close()
	logQueue := utils.NewSimpleQueue(linesCount)
	scanner := bufio.NewScanner(logFile)
	for scanner.Scan() {
		logQueue.Put(scanner.Text())
	}
	Result.Text = fmt.Sprintf("```%s```", strings.Join(logQueue.AsSlice(), "\n"))
	return
}

func (h Handlers) FilesCommandHandler(args string) (Result telegram.OutgoingMessage, err error) {
	Result.ParseMode = MarkdownParseMode
	directoryPath := h.options.Paths.SharePath
	match, errMatch := utils.FilepathHasPrefix(args, fmt.Sprintf("%s/", h.options.Paths.SharePath))
	if args != "" && match && errMatch == nil {
		directoryPath = args
	}
	text, keyboard, err := PrepareFilesListWithKeyboard(directoryPath)
	Result.Text = text
	Result.ReplyMarkup = keyboard
	return
}

func (h Handlers) prepareStatusMessage() (text string, keyboard telegram.InlineKeyboardMarkup, err error) {
	builder := strings.Builder{}
	response, err := h.transmissionClient.GetStatusAll()
	if err != nil {
		return
	}
	if len(response.Arguments.Torrents) == 0 {
		text = "Нет загружаемых торрентов"
		key := telegram.InlineKeyboardButton{
			Text:         updateButtonText,
			CallbackData: updateData,
		}
		keyboard.AddKey(key)
		return
	}
	for _, torrent := range response.Arguments.Torrents {
		StatusMessageText := fmt.Sprintf(StatusTemplate, torrent.Name, torrent.PercentDone*100, utils.BytesCount(torrent.RateDownload), utils.BytesCount(torrent.TotalSize))
		builder.WriteString(StatusMessageText)
		builder.WriteString("\n\n")
		key := telegram.InlineKeyboardButton{
			Text:         torrent.Name,
			CallbackData: fmt.Sprintf(selectTorrentDataTemplate, torrent.ID),
		}
		keyboard.AddKey(key)
	}
	key := telegram.InlineKeyboardButton{
		Text:         updateButtonText,
		CallbackData: updateData,
	}
	keyboard.AddKey(key)
	text = builder.String()
	return
}

func (h Handlers) SelectTorrentCallbackHandler(callback telegram.CallbackQuery) (Result telegram.AnswerCallbackQuery, err error) {
	splittedData := strings.Split(callback.Data, " ")
	if len(splittedData) < 2 {
		err = fmt.Errorf("invalid callback data")
		return
	}
	torrentID, err := strconv.Atoi(splittedData[1])
	if err != nil {
		return
	}
	torrentResult, err := h.transmissionClient.GetTorrent(torrentID)
	if err != nil {
		return
	}
	if len(torrentResult.Arguments.Torrents) != 1 {
		return
	}
	torrent := torrentResult.Arguments.Torrents[0]
	keyboard := telegram.InlineKeyboardMarkup{}
	if torrent.Status == torrentStatusStopped {
		keyResume := telegram.InlineKeyboardButton{
			Text:         resumeButtonText,
			CallbackData: fmt.Sprintf(resumeTorrentDataTemplate, torrentID),
		}
		keyboard.AddKey(keyResume)
	} else {
		keyStop := telegram.InlineKeyboardButton{
			Text:         stopButtonText,
			CallbackData: fmt.Sprintf(stopTorrentDataTemplate, torrentID),
		}
		keyboard.AddKey(keyStop)
	}
	keyDelete := telegram.InlineKeyboardButton{
		Text:         removeButtonText,
		CallbackData: fmt.Sprintf(removeTorrentDataTemplate, torrentID),
	}
	keyboard.AddKey(keyDelete)
	keyBack := telegram.InlineKeyboardButton{
		Text:         backButtonText,
		CallbackData: backData,
	}
	keyboard.AddKey(keyBack)
	message := telegram.EditMessageReplyMarkup{
		ChatID:      callback.Message.Chat.ID,
		MessageID:   callback.Message.MessageID,
		ReplyMarkup: keyboard,
	}
	h.outgoingMessagesChan <- message
	return
}

func (h Handlers) StopTorrentCallbackHandler(callback telegram.CallbackQuery) (Result telegram.AnswerCallbackQuery, err error) {
	torrentID, err := strconv.Atoi(strings.Split(callback.Data, " ")[1])
	if err != nil {
		return
	}
	result, err := h.transmissionClient.StopTorrent(torrentID)
	if err != nil || result.Result != "success" {
		return
	}
	keyboard := telegram.InlineKeyboardMarkup{}
	keyResume := telegram.InlineKeyboardButton{
		Text:         resumeButtonText,
		CallbackData: fmt.Sprintf(resumeTorrentDataTemplate, torrentID),
	}
	keyboard.AddKey(keyResume)
	keyDelete := telegram.InlineKeyboardButton{
		Text:         removeButtonText,
		CallbackData: fmt.Sprintf(removeTorrentDataTemplate, torrentID),
	}
	keyboard.AddKey(keyDelete)
	keyBack := telegram.InlineKeyboardButton{
		Text:         backButtonText,
		CallbackData: backData,
	}
	keyboard.AddKey(keyBack)
	message := telegram.EditMessageReplyMarkup{
		ChatID:      callback.Message.Chat.ID,
		MessageID:   callback.Message.MessageID,
		ReplyMarkup: keyboard,
	}

	h.outgoingMessagesChan <- message
	return
}

func (h Handlers) ResumeTorrentCallbackHandler(callback telegram.CallbackQuery) (Result telegram.AnswerCallbackQuery, err error) {
	torrentID, err := strconv.Atoi(strings.Split(callback.Data, " ")[1])
	if err != nil {
		return
	}
	result, err := h.transmissionClient.StartTorrent(torrentID)
	if err != nil || result.Result != "success" {
		return
	}
	keyboard := telegram.InlineKeyboardMarkup{}
	keyResume := telegram.InlineKeyboardButton{
		Text:         stopButtonText,
		CallbackData: fmt.Sprintf(stopTorrentDataTemplate, torrentID),
	}
	keyboard.AddKey(keyResume)
	keyDelete := telegram.InlineKeyboardButton{
		Text:         removeButtonText,
		CallbackData: fmt.Sprintf(removeTorrentDataTemplate, torrentID),
	}
	keyboard.AddKey(keyDelete)
	keyBack := telegram.InlineKeyboardButton{
		Text:         backButtonText,
		CallbackData: backData,
	}
	keyboard.AddKey(keyBack)
	message := telegram.EditMessageReplyMarkup{
		ChatID:      callback.Message.Chat.ID,
		MessageID:   callback.Message.MessageID,
		ReplyMarkup: keyboard,
	}

	h.outgoingMessagesChan <- message
	return
}

func (h Handlers) RemoveTorrentCallbackHandler(callback telegram.CallbackQuery) (Result telegram.AnswerCallbackQuery, err error) {
	torrentID, err := strconv.Atoi(strings.Split(callback.Data, " ")[1])
	if err != nil {
		return
	}
	_, err = h.transmissionClient.RemoveTorrent([]int{torrentID})
	if err != nil {
		return
	}
	message := telegram.EditMessageText{
		ChatID:    callback.Message.Chat.ID,
		MessageID: callback.Message.MessageID,
	}
	text, keyboard, err := h.prepareStatusMessage()
	if err != nil {
		return
	}
	message.Text = text
	message.ReplyMarkup = keyboard
	h.outgoingMessagesChan <- message
	err = h.dbase.DeleteTorrent(torrentID)
	return
}

func (h Handlers) UpdateTorrentsListCallbackHandler(callback telegram.CallbackQuery) (Result telegram.AnswerCallbackQuery, err error) {
	message := telegram.EditMessageText{
		ChatID:    callback.Message.Chat.ID,
		MessageID: callback.Message.MessageID,
	}
	text, keyboard, err := h.prepareStatusMessage()
	if err != nil {
		return
	}
	message.Text = text
	message.ReplyMarkup = keyboard
	h.outgoingMessagesChan <- message
	return
}

func (h Handlers) SelectFileCallbackHandler(callback telegram.CallbackQuery) (Result telegram.AnswerCallbackQuery, err error) {
	filesRaw := strings.Split(callback.Message.Text, "\n")

	files := make([]string, 0)
	if len(filesRaw) > 2 {
		files = filesRaw[2:]
	}
	directory := filesRaw[0]
	fileID, err := strconv.Atoi(strings.Split(callback.Data, " ")[1])
	if err != nil {
		return
	}
	file := ""
	if fileID == -1 {
		match, errMatch := utils.FilepathHasPrefix(directory, fmt.Sprintf("%s/", h.options.Paths.SharePath))
		if !match || errMatch != nil {
			return
		}
		file = filepath.Dir(directory)
	} else {
		filename := files[fileID] // TODO: перепроверить. Всякое может быть. Вдруг fileID придёт не -1, а список будет пустым?
		file = filepath.Join(directory, filename)
	}

	message := telegram.EditMessageText{
		ChatID:    callback.Message.Chat.ID,
		MessageID: callback.Message.MessageID,
		ParseMode: MarkdownParseMode,
	}
	text, keyboard, err := PrepareFilesListWithKeyboard(file)
	if err != nil {
		return
	}
	message.Text = text
	message.ReplyMarkup = keyboard
	h.outgoingMessagesChan <- message
	return
}

func (h Handlers) RemoveFileCallbackHandler(callback telegram.CallbackQuery) (Result telegram.AnswerCallbackQuery, err error) {
	file := strings.Split(callback.Message.Text, "\n")[0]
	match, errMatch := utils.FilepathHasPrefix(file, fmt.Sprintf("%s/", h.options.Paths.SharePath))
	if !match || errMatch != nil {
		err = fmt.Errorf("invalid path")
		return
	}
	data := strings.Split(callback.Data, " ")
	if len(data) < 2 || data[1] != "1" {
		message := telegram.EditMessageText{
			ChatID:    callback.Message.Chat.ID,
			MessageID: callback.Message.MessageID,
			ParseMode: MarkdownParseMode,
		}
		message.Text = fmt.Sprintf("```\n%s\n\n```", file)
		keyboard := telegram.InlineKeyboardMarkup{}
		key := telegram.InlineKeyboardButton{
			Text:         "Да",
			CallbackData: fmt.Sprintf(deleteFileDataTemplate, 1),
		}
		keyboard.AddKey(key)
		key = telegram.InlineKeyboardButton{
			Text:         "Нет",
			CallbackData: fmt.Sprintf(selectFileDataTemplate, -1),
		}
		keyboard.AddKey(key)
		message.ReplyMarkup = keyboard
		h.outgoingMessagesChan <- message
		return
	}
	err = os.RemoveAll(file)
	if err != nil {
		return
	}
	callback.Data = fmt.Sprintf(selectFileDataTemplate, -1)
	return h.SelectFileCallbackHandler(callback)
}

func (h Handlers) DownloadFileCallbackHandler(callback telegram.CallbackQuery) (Result telegram.AnswerCallbackQuery, err error) {
	file := strings.Split(callback.Message.Text, "\n")[0]
	match, errMatch := utils.FilepathHasPrefix(file, fmt.Sprintf("%s/", h.options.Paths.SharePath))
	if !match || errMatch != nil {
		err = fmt.Errorf("invalid path")
		return
	}
	message := telegram.SendDocument{
		ChatID:   callback.Message.Chat.ID,
		FileName: file,
	}
	h.outgoingMessagesChan <- message
	return
}
