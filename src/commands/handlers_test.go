package commands

import (
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"path"
	"path/filepath"
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/bziaks/new-torrent-bot/src/config"
	"gitlab.com/bziaks/new-torrent-bot/src/db"
	"gitlab.com/bziaks/new-torrent-bot/src/telegram"
	"gitlab.com/bziaks/new-torrent-bot/src/transmission"
)

func TestHandlers_FilesCommandHandler(t *testing.T) {
	wd, err := os.Getwd()
	require.Nil(t, err)
	testDir := path.Join(wd, "test", "testSub")
	err = os.MkdirAll(testDir, os.ModePerm)
	require.Nil(t, err)

	trc := &transmission.Client{}
	outc := make(chan telegram.MessageSenderI, 10)
	dbase := &db.DBLite{}
	options := config.Options{
		Paths: config.PathOptions{SharePath: wd},
	}
	h := NewHandlers(trc, outc, dbase, options)

	result, err := h.FilesCommandHandler("")
	require.Nil(t, err)
	splittedResult := strings.Split(result.Text, "\n")
	require.Equal(t, "```", splittedResult[0])
	require.Equal(t, wd, splittedResult[1])
	require.Equal(t, "```", splittedResult[len(splittedResult)-1])
	require.Contains(t, splittedResult, "test")
	require.Equal(t, len(splittedResult)-2, len(result.ReplyMarkup.InlineKeyboard))
	for _, key := range result.ReplyMarkup.InlineKeyboard {
		if key[0].Text == removeButtonText || key[0].Text == upButtonText {
			continue
		}
		require.Contains(t, splittedResult, key[0].Text)
	}

	result, err = h.FilesCommandHandler(testDir)
	require.Nil(t, err)
	splittedResult = strings.Split(result.Text, "\n")
	require.Equal(t, "```", splittedResult[0])
	require.Equal(t, testDir, splittedResult[1])
	require.Equal(t, "```", splittedResult[len(splittedResult)-1])
	require.Equal(t, 4, len(splittedResult))
	require.Equal(t, 2, len(result.ReplyMarkup.InlineKeyboard))

	err = os.RemoveAll(filepath.Dir(testDir))
	require.Nil(t, err)
}

func TestHandlers_LogCommandHandler(t *testing.T) {
	logPath, err := os.Getwd()
	require.Nil(t, err)
	logFile := filepath.Join(logPath, "test.log")

	data := `its
just
test
log
or
not log
trying to read logs`

	err = ioutil.WriteFile(logFile, []byte(data), os.ModePerm)
	require.Nil(t, err)

	trc := &transmission.Client{}
	outc := make(chan telegram.MessageSenderI, 10)
	dbase := &db.DBLite{}
	options := config.Options{
		Paths: config.PathOptions{LogPath: logFile},
	}
	h := NewHandlers(trc, outc, dbase, options)

	result, err := h.LogCommandHandler("7")
	require.Nil(t, err)
	strings.HasPrefix(result.Text, "```")
	strings.HasSuffix(result.Text, "```")
	require.Equal(t, MarkdownParseMode, result.ParseMode)
	preparedResult := strings.Trim(result.Text, "`\n")
	require.Equal(t, data, preparedResult)

	result, err = h.LogCommandHandler("15")
	require.Nil(t, err)
	strings.HasPrefix(result.Text, "```")
	strings.HasSuffix(result.Text, "```")
	require.Equal(t, MarkdownParseMode, result.ParseMode)
	preparedResult = strings.Trim(result.Text, "`\n")
	require.Equal(t, data, preparedResult)

	result, err = h.LogCommandHandler("3")
	require.Nil(t, err)
	strings.HasPrefix(result.Text, "```")
	strings.HasSuffix(result.Text, "```")
	require.Equal(t, MarkdownParseMode, result.ParseMode)
	preparedResult = strings.Trim(result.Text, "`\n")
	data = `or
not log
trying to read logs`
	require.Equal(t, data, preparedResult)

	err = os.Remove(logFile)
	require.Nil(t, err)
}

func TestHandlers_StatusCommandHandler_ExistingTorrent(t *testing.T) {
	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		_, err := io.WriteString(w, `{"result": "success", "arguments": {"torrents": [{"id": 1, "name": "Test torrent", "totalSize": 102421, "status": 0, "percentDone": 0.69123, "rateDownload": 10240}]}}`)
		require.Nil(t, err)
	}))
	defer server.Close()

	trc := transmission.New(server.URL)
	outc := make(chan telegram.MessageSenderI, 10)
	dbase := &db.DBLite{}
	options := config.Options{}
	h := NewHandlers(trc, outc, dbase, options)

	result, err := h.StatusCommandHandler("")
	require.Nil(t, err)
	splittedResult := strings.Split(result.Text, "\n")
	require.Equal(t, "Имя: Test torrent", splittedResult[0])
	require.Equal(t, "Процентов скачано: 69.123%", splittedResult[1])
	require.Equal(t, "Скорость скачивания: 10.000 KB/s", splittedResult[2])
	require.Equal(t, "Общий размер: 100.021 KB", splittedResult[3])
	require.Equal(t, 2, len(result.ReplyMarkup.InlineKeyboard))
	require.Equal(t, "Test torrent", result.ReplyMarkup.InlineKeyboard[0][0].Text)
	require.Equal(t, fmt.Sprintf(selectTorrentDataTemplate, 1), result.ReplyMarkup.InlineKeyboard[0][0].CallbackData)
	require.Equal(t, updateButtonText, result.ReplyMarkup.InlineKeyboard[1][0].Text)
	require.Equal(t, updateData, result.ReplyMarkup.InlineKeyboard[1][0].CallbackData)
}

func TestHandlers_StatusCommandHandler_Error(t *testing.T) {
	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusBadGateway)
	}))
	defer server.Close()

	trc := transmission.New(server.URL)
	outc := make(chan telegram.MessageSenderI, 10)
	dbase := &db.DBLite{}
	options := config.Options{}
	h := NewHandlers(trc, outc, dbase, options)

	_, err := h.StatusCommandHandler("")
	require.NotNil(t, err)
}

func TestHandlers_StatusCommandHandler_NoTorrents(t *testing.T) {
	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		_, err := io.WriteString(w, `{"result": "success", "arguments": {"torrents": []}}`)
		require.Nil(t, err)
	}))
	defer server.Close()

	trc := transmission.New(server.URL)
	outc := make(chan telegram.MessageSenderI, 10)
	dbase := &db.DBLite{}
	options := config.Options{}
	h := NewHandlers(trc, outc, dbase, options)

	result, err := h.StatusCommandHandler("")
	require.Nil(t, err)
	require.Equal(t, "Нет загружаемых торрентов", result.Text)
	require.Equal(t, 1, len(result.ReplyMarkup.InlineKeyboard))
	require.Equal(t, updateButtonText, result.ReplyMarkup.InlineKeyboard[0][0].Text)
	require.Equal(t, updateData, result.ReplyMarkup.InlineKeyboard[0][0].CallbackData)
}

func prepareCallback(callbackData string) (cq telegram.CallbackQuery) {
	cq = telegram.CallbackQuery{
		ID: "asdf",
		Message: telegram.Message{
			MessageID: 1,
			From:      telegram.User{},
			Date:      0,
			Chat: telegram.Chat{
				ID:       1,
				Username: "asd",
			},
			Text: "",
		},
		Data: callbackData,
	}
	return
}

func TestHandlers_SelectTorrentCallbackHandler_TorrentStopped(t *testing.T) {
	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		_, err := io.WriteString(w, `{"result": "success", "arguments": {"torrents": [{"id": 1, "name": "Test torrent", "totalSize": 102421, "status": 0, "percentDone": 0.69123, "rateDownload": 10240}]}}`)
		require.Nil(t, err)
	}))
	defer server.Close()

	c := Callbacks{}
	trc := transmission.New(server.URL)
	outc := make(chan telegram.MessageSenderI, 10)
	dbase := &db.DBLite{}
	options := config.Options{}
	h := NewHandlers(trc, outc, dbase, options)
	c.Add("select-torrent", h.SelectTorrentCallbackHandler)

	callback := prepareCallback(fmt.Sprintf(selectTorrentDataTemplate, 1))

	result, err := c.AnswerCallback(callback)
	require.Nil(t, err)
	require.Equal(t, callback.ID, result.CallbackQueryID)
	require.Equal(t, "", result.Text)
	select {
	case message := <-outc:
		parsedMessage, ok := message.(telegram.EditMessageReplyMarkup)
		require.True(t, ok)
		require.Equal(t, callback.Message.Chat.ID, parsedMessage.ChatID)
		require.Equal(t, callback.Message.MessageID, parsedMessage.MessageID)
		require.Equal(t, 3, len(parsedMessage.ReplyMarkup.InlineKeyboard))
		require.Equal(t, resumeButtonText, parsedMessage.ReplyMarkup.InlineKeyboard[0][0].Text)
		require.Equal(t, fmt.Sprintf(resumeTorrentDataTemplate, 1), parsedMessage.ReplyMarkup.InlineKeyboard[0][0].CallbackData)
		require.Equal(t, removeButtonText, parsedMessage.ReplyMarkup.InlineKeyboard[1][0].Text)
		require.Equal(t, fmt.Sprintf(removeTorrentDataTemplate, 1), parsedMessage.ReplyMarkup.InlineKeyboard[1][0].CallbackData)
		require.Equal(t, backButtonText, parsedMessage.ReplyMarkup.InlineKeyboard[2][0].Text)
		require.Equal(t, fmt.Sprintf(backData), parsedMessage.ReplyMarkup.InlineKeyboard[2][0].CallbackData)
	default:
		t.Error("Empty channel")
	}
}

func TestHandlers_SelectTorrentCallbackHandler_TorrentRunning(t *testing.T) {
	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		_, err := io.WriteString(w, `{"result": "success", "arguments": {"torrents": [{"id": 1, "name": "Test torrent", "totalSize": 102421, "status": 4, "percentDone": 0.69123, "rateDownload": 10240}]}}`)
		require.Nil(t, err)
	}))
	defer server.Close()

	c := Callbacks{}
	trc := transmission.New(server.URL)
	outc := make(chan telegram.MessageSenderI, 10)
	dbase := &db.DBLite{}
	options := config.Options{}
	h := NewHandlers(trc, outc, dbase, options)
	c.Add("select-torrent", h.SelectTorrentCallbackHandler)

	callback := prepareCallback(fmt.Sprintf(selectTorrentDataTemplate, 1))

	result, err := c.AnswerCallback(callback)
	require.Nil(t, err)
	require.Equal(t, callback.ID, result.CallbackQueryID)
	require.Equal(t, "", result.Text)
	select {
	case message := <-outc:
		parsedMessage, ok := message.(telegram.EditMessageReplyMarkup)
		require.True(t, ok)
		require.Equal(t, callback.Message.Chat.ID, parsedMessage.ChatID)
		require.Equal(t, callback.Message.MessageID, parsedMessage.MessageID)
		require.Equal(t, 3, len(parsedMessage.ReplyMarkup.InlineKeyboard))
		require.Equal(t, stopButtonText, parsedMessage.ReplyMarkup.InlineKeyboard[0][0].Text)
		require.Equal(t, fmt.Sprintf(stopTorrentDataTemplate, 1), parsedMessage.ReplyMarkup.InlineKeyboard[0][0].CallbackData)
		require.Equal(t, removeButtonText, parsedMessage.ReplyMarkup.InlineKeyboard[1][0].Text)
		require.Equal(t, fmt.Sprintf(removeTorrentDataTemplate, 1), parsedMessage.ReplyMarkup.InlineKeyboard[1][0].CallbackData)
		require.Equal(t, backButtonText, parsedMessage.ReplyMarkup.InlineKeyboard[2][0].Text)
		require.Equal(t, fmt.Sprintf(backData), parsedMessage.ReplyMarkup.InlineKeyboard[2][0].CallbackData)
	default:
		t.Error("Empty channel")
	}
}

func TestHandlers_UpdateTorrentsListCallbackHandler(t *testing.T) {
	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		_, err := io.WriteString(w, `{"result": "success", "arguments": {"torrents": [{"id": 1, "name": "Test torrent", "totalSize": 102421, "status": 4, "percentDone": 0.69123, "rateDownload": 10240}]}}`)
		require.Nil(t, err)
	}))
	defer server.Close()

	c := Callbacks{}
	trc := transmission.New(server.URL)
	outc := make(chan telegram.MessageSenderI, 10)
	dbase := &db.DBLite{}
	options := config.Options{}
	h := NewHandlers(trc, outc, dbase, options)
	c.Add(backData, h.UpdateTorrentsListCallbackHandler)

	callback := prepareCallback(backData)

	result, err := c.AnswerCallback(callback)
	require.Nil(t, err)
	require.Equal(t, callback.ID, result.CallbackQueryID)
	require.Equal(t, "", result.Text)
	select {
	case message := <-outc:
		parsedMessage, ok := message.(telegram.EditMessageText)
		require.True(t, ok)
		require.Equal(t, callback.Message.Chat.ID, parsedMessage.ChatID)
		require.Equal(t, callback.Message.MessageID, parsedMessage.MessageID)
		splittedResult := strings.Split(parsedMessage.Text, "\n")
		require.Equal(t, "Имя: Test torrent", splittedResult[0])
		require.Equal(t, "Процентов скачано: 69.123%", splittedResult[1])
		require.Equal(t, "Скорость скачивания: 10.000 KB/s", splittedResult[2])
		require.Equal(t, "Общий размер: 100.021 KB", splittedResult[3])
		require.Equal(t, 2, len(parsedMessage.ReplyMarkup.InlineKeyboard))
		require.Equal(t, "Test torrent", parsedMessage.ReplyMarkup.InlineKeyboard[0][0].Text)
		require.Equal(t, fmt.Sprintf(selectTorrentDataTemplate, 1), parsedMessage.ReplyMarkup.InlineKeyboard[0][0].CallbackData)
		require.Equal(t, updateButtonText, parsedMessage.ReplyMarkup.InlineKeyboard[1][0].Text)
		require.Equal(t, updateData, parsedMessage.ReplyMarkup.InlineKeyboard[1][0].CallbackData)
	default:
		t.Error("Empty channel")
	}
}

func TestHandlers_StopTorrentCallbackHandler(t *testing.T) {
	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		_, err := io.WriteString(w, `{"result": "success", "arguments": {"torrents": [{"id": 1, "name": "Test torrent", "totalSize": 102421, "status": 4, "percentDone": 0.69123, "rateDownload": 10240}]}}`)
		require.Nil(t, err)
	}))
	defer server.Close()

	c := Callbacks{}
	trc := transmission.New(server.URL)
	outc := make(chan telegram.MessageSenderI, 10)
	dbase := &db.DBLite{}
	options := config.Options{}
	h := NewHandlers(trc, outc, dbase, options)
	c.Add("stop-torrent", h.StopTorrentCallbackHandler)

	callback := prepareCallback(fmt.Sprintf(stopTorrentDataTemplate, 1))

	result, err := c.AnswerCallback(callback)
	require.Nil(t, err)
	require.Equal(t, callback.ID, result.CallbackQueryID)
	require.Equal(t, "", result.Text)
	select {
	case message := <-outc:
		parsedMessage, ok := message.(telegram.EditMessageReplyMarkup)
		require.True(t, ok)
		require.Equal(t, callback.Message.Chat.ID, parsedMessage.ChatID)
		require.Equal(t, callback.Message.MessageID, parsedMessage.MessageID)
		require.Equal(t, 3, len(parsedMessage.ReplyMarkup.InlineKeyboard))
		require.Equal(t, resumeButtonText, parsedMessage.ReplyMarkup.InlineKeyboard[0][0].Text)
		require.Equal(t, fmt.Sprintf(resumeTorrentDataTemplate, 1), parsedMessage.ReplyMarkup.InlineKeyboard[0][0].CallbackData)
		require.Equal(t, removeButtonText, parsedMessage.ReplyMarkup.InlineKeyboard[1][0].Text)
		require.Equal(t, fmt.Sprintf(removeTorrentDataTemplate, 1), parsedMessage.ReplyMarkup.InlineKeyboard[1][0].CallbackData)
		require.Equal(t, backButtonText, parsedMessage.ReplyMarkup.InlineKeyboard[2][0].Text)
		require.Equal(t, fmt.Sprintf(backData), parsedMessage.ReplyMarkup.InlineKeyboard[2][0].CallbackData)
	default:
		t.Error("Empty channel")
	}
}

func TestHandlers_ResumeTorrentCallbackHandler(t *testing.T) {
	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		_, err := io.WriteString(w, `{"result": "success", "arguments": {"torrents": [{"id": 1, "name": "Test torrent", "totalSize": 102421, "status": 4, "percentDone": 0.69123, "rateDownload": 10240}]}}`)
		require.Nil(t, err)
	}))
	defer server.Close()

	c := Callbacks{}
	trc := transmission.New(server.URL)
	outc := make(chan telegram.MessageSenderI, 10)
	dbase := &db.DBLite{}
	options := config.Options{}
	h := NewHandlers(trc, outc, dbase, options)
	c.Add("resume-torrent", h.ResumeTorrentCallbackHandler)

	callback := prepareCallback(fmt.Sprintf(resumeTorrentDataTemplate, 1))

	result, err := c.AnswerCallback(callback)
	require.Nil(t, err)
	require.Equal(t, callback.ID, result.CallbackQueryID)
	require.Equal(t, "", result.Text)
	select {
	case message := <-outc:
		parsedMessage, ok := message.(telegram.EditMessageReplyMarkup)
		require.True(t, ok)
		require.Equal(t, callback.Message.Chat.ID, parsedMessage.ChatID)
		require.Equal(t, callback.Message.MessageID, parsedMessage.MessageID)
		require.Equal(t, 3, len(parsedMessage.ReplyMarkup.InlineKeyboard))
		require.Equal(t, stopButtonText, parsedMessage.ReplyMarkup.InlineKeyboard[0][0].Text)
		require.Equal(t, fmt.Sprintf(stopTorrentDataTemplate, 1), parsedMessage.ReplyMarkup.InlineKeyboard[0][0].CallbackData)
		require.Equal(t, removeButtonText, parsedMessage.ReplyMarkup.InlineKeyboard[1][0].Text)
		require.Equal(t, fmt.Sprintf(removeTorrentDataTemplate, 1), parsedMessage.ReplyMarkup.InlineKeyboard[1][0].CallbackData)
		require.Equal(t, backButtonText, parsedMessage.ReplyMarkup.InlineKeyboard[2][0].Text)
		require.Equal(t, fmt.Sprintf(backData), parsedMessage.ReplyMarkup.InlineKeyboard[2][0].CallbackData)
	default:
		t.Error("Empty channel")
	}
}

func TestHandlers_RemoveTorrentCallbackHandler(t *testing.T) {
	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		_, err := io.WriteString(w, `{"result": "success", "arguments": {"torrents": [{"id": 1, "name": "Test torrent", "totalSize": 102421, "status": 4, "percentDone": 0.69123, "rateDownload": 10240}]}}`)
		require.Nil(t, err)
	}))
	defer server.Close()

	c := Callbacks{}
	trc := transmission.New(server.URL)
	outc := make(chan telegram.MessageSenderI, 10)
	dbase, err := db.New("torrents-test.leveldb")
	require.Nil(t, err)
	err = dbase.AddTorrent(1, 1)
	require.Nil(t, err)
	options := config.Options{}
	h := NewHandlers(trc, outc, dbase, options)
	c.Add("delete-torrent", h.RemoveTorrentCallbackHandler)

	callback := prepareCallback(fmt.Sprintf(removeTorrentDataTemplate, 1))
	result, err := c.AnswerCallback(callback)
	require.Nil(t, err)
	require.Equal(t, callback.ID, result.CallbackQueryID)
	require.Equal(t, "", result.Text)

	select {
	case message := <-outc:
		parsedMessage, ok := message.(telegram.EditMessageText)
		require.True(t, ok)
		require.Equal(t, callback.Message.Chat.ID, parsedMessage.ChatID)
		require.Equal(t, callback.Message.MessageID, parsedMessage.MessageID)
		splittedResult := strings.Split(parsedMessage.Text, "\n")
		require.Equal(t, "Имя: Test torrent", splittedResult[0])
		require.Equal(t, "Процентов скачано: 69.123%", splittedResult[1])
		require.Equal(t, "Скорость скачивания: 10.000 KB/s", splittedResult[2])
		require.Equal(t, "Общий размер: 100.021 KB", splittedResult[3])
		require.Equal(t, 2, len(parsedMessage.ReplyMarkup.InlineKeyboard))
		require.Equal(t, "Test torrent", parsedMessage.ReplyMarkup.InlineKeyboard[0][0].Text)
		require.Equal(t, fmt.Sprintf(selectTorrentDataTemplate, 1), parsedMessage.ReplyMarkup.InlineKeyboard[0][0].CallbackData)
		require.Equal(t, updateButtonText, parsedMessage.ReplyMarkup.InlineKeyboard[1][0].Text)
		require.Equal(t, updateData, parsedMessage.ReplyMarkup.InlineKeyboard[1][0].CallbackData)
	default:
		t.Error("Empty channel")
	}
	err = os.RemoveAll("torrents-test.leveldb")
	require.Nil(t, err)
}

func TestHandlers_SelectFileCallbackHandler(t *testing.T) {
	wd, err := os.Getwd()
	require.Nil(t, err)
	testDir := path.Join(wd, "test", "testSub")
	err = os.MkdirAll(testDir, os.ModePerm)
	require.Nil(t, err)

	c := Callbacks{}
	trc := &transmission.Client{}
	outc := make(chan telegram.MessageSenderI, 10)
	dbase := &db.DBLite{}
	options := config.Options{
		Paths: config.PathOptions{SharePath: wd},
	}
	h := NewHandlers(trc, outc, dbase, options)
	c.Add("select-file", h.SelectFileCallbackHandler)

	callback := telegram.CallbackQuery{
		ID: "asdf",
		Message: telegram.Message{
			MessageID: 1,
			From:      telegram.User{},
			Date:      0,
			Chat: telegram.Chat{
				ID:       1,
				Username: "asd",
			},
			Text: fmt.Sprintf("%s\n\n%s", wd, "test"),
		},
		Data: fmt.Sprintf(selectFileDataTemplate, 0),
	}
	result, err := c.AnswerCallback(callback)
	require.Nil(t, err)
	require.Equal(t, callback.ID, result.CallbackQueryID)
	require.Equal(t, "", result.Text)
	select {
	case message := <-outc:
		parsedMessage, ok := message.(telegram.EditMessageText)
		require.True(t, ok)
		require.Equal(t, callback.Message.Chat.ID, parsedMessage.ChatID)
		require.Equal(t, callback.Message.MessageID, parsedMessage.MessageID)
		splittedResult := strings.Split(strings.Trim(parsedMessage.Text, "`\n"), "\n")
		require.Equal(t, filepath.Join(wd, "test"), splittedResult[0])
		require.Equal(t, "", splittedResult[1])
		require.Equal(t, "testSub", splittedResult[2])
		require.Equal(t, 3, len(parsedMessage.ReplyMarkup.InlineKeyboard))
		require.Equal(t, removeButtonText, parsedMessage.ReplyMarkup.InlineKeyboard[0][0].Text)
		require.Equal(t, fmt.Sprintf(deleteFileDataTemplate, -1), parsedMessage.ReplyMarkup.InlineKeyboard[0][0].CallbackData)
		require.Equal(t, upButtonText, parsedMessage.ReplyMarkup.InlineKeyboard[1][0].Text)
		require.Equal(t, fmt.Sprintf(selectFileDataTemplate, -1), parsedMessage.ReplyMarkup.InlineKeyboard[1][0].CallbackData)
		require.Equal(t, "testSub", parsedMessage.ReplyMarkup.InlineKeyboard[2][0].Text)
		require.Equal(t, fmt.Sprintf(selectFileDataTemplate, 0), parsedMessage.ReplyMarkup.InlineKeyboard[2][0].CallbackData)
	default:
		t.Error("Empty channel")
	}

	err = os.RemoveAll(filepath.Dir(testDir))
	require.Nil(t, err)
}

func TestHandlers_SelectFileCallbackHandler_Up(t *testing.T) {
	wd, err := os.Getwd()
	require.Nil(t, err)
	testDir := path.Join(wd, "test", "testSub")
	err = os.MkdirAll(testDir, os.ModePerm)
	require.Nil(t, err)

	c := Callbacks{}
	trc := &transmission.Client{}
	outc := make(chan telegram.MessageSenderI, 10)
	dbase := &db.DBLite{}
	options := config.Options{
		Paths: config.PathOptions{SharePath: wd},
	}
	h := NewHandlers(trc, outc, dbase, options)
	c.Add("select-file", h.SelectFileCallbackHandler)

	callback := telegram.CallbackQuery{
		ID: "asdf",
		Message: telegram.Message{
			MessageID: 1,
			From:      telegram.User{},
			Date:      0,
			Chat: telegram.Chat{
				ID:       1,
				Username: "asd",
			},
			Text: fmt.Sprintf("%s", filepath.Join(wd, "test", "testSub")),
		},
		Data: fmt.Sprintf(selectFileDataTemplate, -1),
	}
	result, err := c.AnswerCallback(callback)
	require.Nil(t, err)
	require.Equal(t, callback.ID, result.CallbackQueryID)
	require.Equal(t, "", result.Text)
	select {
	case message := <-outc:
		parsedMessage, ok := message.(telegram.EditMessageText)
		require.True(t, ok)
		require.Equal(t, callback.Message.Chat.ID, parsedMessage.ChatID)
		require.Equal(t, callback.Message.MessageID, parsedMessage.MessageID)
		splittedResult := strings.Split(strings.Trim(parsedMessage.Text, "`\n"), "\n")
		require.Equal(t, filepath.Join(wd, "test"), splittedResult[0])
		require.Equal(t, "", splittedResult[1])
		require.Equal(t, "testSub", splittedResult[2])
		require.Equal(t, 3, len(parsedMessage.ReplyMarkup.InlineKeyboard))
		require.Equal(t, removeButtonText, parsedMessage.ReplyMarkup.InlineKeyboard[0][0].Text)
		require.Equal(t, fmt.Sprintf(deleteFileDataTemplate, -1), parsedMessage.ReplyMarkup.InlineKeyboard[0][0].CallbackData)
		require.Equal(t, upButtonText, parsedMessage.ReplyMarkup.InlineKeyboard[1][0].Text)
		require.Equal(t, fmt.Sprintf(selectFileDataTemplate, -1), parsedMessage.ReplyMarkup.InlineKeyboard[1][0].CallbackData)
		require.Equal(t, "testSub", parsedMessage.ReplyMarkup.InlineKeyboard[2][0].Text)
		require.Equal(t, fmt.Sprintf(selectFileDataTemplate, 0), parsedMessage.ReplyMarkup.InlineKeyboard[2][0].CallbackData)
	default:
		t.Error("Empty channel")
	}

	err = os.RemoveAll(filepath.Dir(testDir))
	require.Nil(t, err)
}

func TestHandlers_RemoveFileCallbackHandler(t *testing.T) {
	wd, err := os.Getwd()
	require.Nil(t, err)
	testDir := path.Join(wd, "test", "testSub")
	err = os.MkdirAll(testDir, os.ModePerm)
	require.Nil(t, err)

	c := Callbacks{}
	trc := &transmission.Client{}
	outc := make(chan telegram.MessageSenderI, 10)
	dbase := &db.DBLite{}
	options := config.Options{
		Paths: config.PathOptions{SharePath: wd},
	}
	h := NewHandlers(trc, outc, dbase, options)
	c.Add("delete-file", h.RemoveFileCallbackHandler)

	callback := telegram.CallbackQuery{
		ID: "asdf",
		Message: telegram.Message{
			MessageID: 1,
			From:      telegram.User{},
			Date:      0,
			Chat: telegram.Chat{
				ID:       1,
				Username: "asd",
			},
			Text: fmt.Sprintf("%s", filepath.Join(wd, "test", "testSub")),
		},
		Data: fmt.Sprintf(deleteFileDataTemplate, -1),
	}
	result, err := c.AnswerCallback(callback)
	require.Nil(t, err)
	require.Equal(t, callback.ID, result.CallbackQueryID)
	require.Equal(t, "", result.Text)
	select {
	case message := <-outc:
		parsedMessage, ok := message.(telegram.EditMessageText)
		require.True(t, ok)
		require.Equal(t, callback.Message.Chat.ID, parsedMessage.ChatID)
		require.Equal(t, callback.Message.MessageID, parsedMessage.MessageID)
		splittedResult := strings.Split(strings.Trim(parsedMessage.Text, "`\n"), "\n")
		require.Equal(t, filepath.Join(wd, "test", "testSub"), splittedResult[0])
		require.Equal(t, 2, len(parsedMessage.ReplyMarkup.InlineKeyboard))
		require.Equal(t, "Да", parsedMessage.ReplyMarkup.InlineKeyboard[0][0].Text)
		require.Equal(t, fmt.Sprintf(deleteFileDataTemplate, 1), parsedMessage.ReplyMarkup.InlineKeyboard[0][0].CallbackData)
		require.Equal(t, "Нет", parsedMessage.ReplyMarkup.InlineKeyboard[1][0].Text)
		require.Equal(t, fmt.Sprintf(selectFileDataTemplate, -1), parsedMessage.ReplyMarkup.InlineKeyboard[1][0].CallbackData)
	default:
		t.Error("Empty channel")
	}

	err = os.RemoveAll(filepath.Dir(testDir))
	require.Nil(t, err)
}

func TestHandlers_RemoveFileCallbackHandler_Yes(t *testing.T) {
	wd, err := os.Getwd()
	require.Nil(t, err)
	testDir := path.Join(wd, "test", "testSub")
	err = os.MkdirAll(testDir, os.ModePerm)
	require.Nil(t, err)

	c := Callbacks{}
	trc := &transmission.Client{}
	outc := make(chan telegram.MessageSenderI, 10)
	dbase := &db.DBLite{}
	options := config.Options{
		Paths: config.PathOptions{SharePath: wd},
	}
	h := NewHandlers(trc, outc, dbase, options)
	c.Add("delete-file", h.RemoveFileCallbackHandler)

	callback := telegram.CallbackQuery{
		ID: "asdf",
		Message: telegram.Message{
			MessageID: 1,
			From:      telegram.User{},
			Date:      0,
			Chat: telegram.Chat{
				ID:       1,
				Username: "asd",
			},
			Text: fmt.Sprintf("%s", filepath.Join(wd, "test", "testSub")),
		},
		Data: fmt.Sprintf(deleteFileDataTemplate, 1),
	}
	result, err := c.AnswerCallback(callback)
	require.Nil(t, err)
	require.Equal(t, callback.ID, result.CallbackQueryID)
	require.Equal(t, "", result.Text)
	_, err = os.Stat(filepath.Join(wd, "test", "testSub"))
	require.NotNil(t, err)
	require.True(t, os.IsNotExist(err))
	select {
	case message := <-outc:
		parsedMessage, ok := message.(telegram.EditMessageText)
		require.True(t, ok)
		require.Equal(t, callback.Message.Chat.ID, parsedMessage.ChatID)
		require.Equal(t, callback.Message.MessageID, parsedMessage.MessageID)
		splittedResult := strings.Split(strings.Trim(parsedMessage.Text, "`\n"), "\n")
		require.Equal(t, filepath.Join(wd, "test"), splittedResult[0])
		require.Equal(t, 2, len(parsedMessage.ReplyMarkup.InlineKeyboard))
		require.Equal(t, removeButtonText, parsedMessage.ReplyMarkup.InlineKeyboard[0][0].Text)
		require.Equal(t, fmt.Sprintf(deleteFileDataTemplate, -1), parsedMessage.ReplyMarkup.InlineKeyboard[0][0].CallbackData)
		require.Equal(t, upButtonText, parsedMessage.ReplyMarkup.InlineKeyboard[1][0].Text)
		require.Equal(t, fmt.Sprintf(selectFileDataTemplate, -1), parsedMessage.ReplyMarkup.InlineKeyboard[1][0].CallbackData)
	default:
		t.Error("Empty channel")
	}

	err = os.RemoveAll(filepath.Dir(testDir))
	require.Nil(t, err)
}
