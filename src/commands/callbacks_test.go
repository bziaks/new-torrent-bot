package commands

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/bziaks/new-torrent-bot/src/telegram"
)

func TestCallbacks(t *testing.T) {
	expected := func(callback telegram.CallbackQuery) (Result telegram.AnswerCallbackQuery, err error) {
		err = fmt.Errorf("some test error")
		return
	}
	c := NewCallbacks()
	c.Add("a", expected)
	actual, ok := c.Get("a")
	require.True(t, ok)
	_, expectedErr := expected(telegram.CallbackQuery{})
	_, actualErr := actual(telegram.CallbackQuery{})
	require.NotNil(t, expectedErr)
	require.NotNil(t, actualErr)
	require.Equal(t, expectedErr.Error(), actualErr.Error())
	_, ok = c.Get("asdf")
	require.False(t, ok)
	callback := telegram.CallbackQuery{
		ID:              "0",
		From:            telegram.User{},
		Message:         telegram.Message{},
		InlineMessageID: "0",
		ChatInstance:    "0",
		Data:            "a",
	}
	_, actualErr = c.AnswerCallback(callback)
	require.NotNil(t, actualErr)
	require.Equal(t, expectedErr.Error(), actualErr.Error())
	callback1 := telegram.CallbackQuery{
		ID:              "0",
		From:            telegram.User{},
		Message:         telegram.Message{},
		InlineMessageID: "0",
		ChatInstance:    "0",
		Data:            "aasd",
	}
	_, actualErr = c.AnswerCallback(callback1)
	require.NotNil(t, actualErr)
	require.Equal(t, "can't find callback", actualErr.Error())
}
