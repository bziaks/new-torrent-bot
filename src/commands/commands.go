package commands

import (
	"strings"

	"gitlab.com/bziaks/new-torrent-bot/src/telegram"
)

type Commands map[string]func(args string) (Result telegram.OutgoingMessage, err error)

func NewCommands() Commands {
	return Commands{}
}

func (c Commands) Add(command string, handler func(args string) (Result telegram.OutgoingMessage, err error)) {
	c[command] = handler
}

func (c Commands) Get(command string) (handler func(args string) (Result telegram.OutgoingMessage, err error), ok bool) {
	handler, ok = c[command]
	return
}

func (c Commands) HandleMessage(message telegram.Message) (Result telegram.OutgoingMessage, err error) {
	parsedText := strings.Split(message.Text, " ")
	command := parsedText[0]
	args := strings.Join(parsedText[1:], " ")
	if command[:1] != "/" {
		return
	}
	if handler, ok := c[command]; ok {
		Result, err = handler(args)
	} else {
		Result.Text = "Неизвестная команда"
	}
	Result.ChatID = message.Chat.ID
	return
}
