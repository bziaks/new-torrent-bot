package main

import (
	"flag"
	"log"
	"os"
	"path"
	"sync"

	"gitlab.com/bziaks/new-torrent-bot/src/config"
	"gitlab.com/bziaks/new-torrent-bot/src/db"
	"gitlab.com/bziaks/new-torrent-bot/src/workers"
)

func main() {
	defaultConfigDir, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}
	defaultConfigPath := path.Join(defaultConfigDir, "config.yml")
	configPath := flag.String("c", defaultConfigPath, "Config path")
	flag.Parse()
	options, err := config.LoadConfig(*configPath)
	if err != nil {
		log.Fatal(err)
	}
	dbase, err := db.New(options.DB.Filename)
	if err != nil {
		panic(err)
	}
	defer dbase.Close()
	preparedWorkers := workers.NewWorkers(options, dbase)
	wg := sync.WaitGroup{}
	wg.Add(len(preparedWorkers))
	for _, worker := range preparedWorkers {
		go worker.Run(&wg)
	}
	wg.Wait()
}
