#!/usr/bin/env python3

import os
import logging
import requests

URL = 'http://127.0.0.1:8090/torrent-done'


def send_request(url: str, torrent_id: int, torrent_name: str) -> bool:
    data = {
        'id': torrent_id,
        'name': torrent_name
    }
    r = requests.post(url, json=data)
    if r.ok and r.status_code == 202:
        return True
    return False


if __name__ == '__main__':
    torrent_id = int(os.getenv('TR_TORRENT_ID'))
    torrent_name = os.getenv('TR_TORRENT_NAME')
    if not send_request(URL, torrent_id, torrent_name):
        logging.info('error while sending request')
