package db

import (
	"strconv"

	"github.com/syndtr/goleveldb/leveldb"
)

const (
	updateIDKey = "update_id"
)

func getIntFromBytes(byteArr []byte) (int, error) {
	return strconv.Atoi(string(byteArr))
}

type DBLite struct {
	dbLevel *leveldb.DB
}

func (database DBLite) GetLastUpdate() (int, error) {
	updateID, err := database.dbLevel.Get([]byte(updateIDKey), nil)
	if err != nil {
		return 0, err
	}
	return getIntFromBytes(updateID)
}

func (database DBLite) UpdateLastUpdateID(updateID int) error {
	err := database.dbLevel.Put([]byte(updateIDKey), []byte(strconv.Itoa(updateID)), nil)
	return err
}

func (database DBLite) AddTorrent(chatID, torrentID int) error {
	err := database.dbLevel.Put([]byte(strconv.Itoa(torrentID)), []byte(strconv.Itoa(chatID)), nil)
	return err
}

func (database DBLite) DeleteTorrent(torrentID int) error {
	err := database.dbLevel.Delete([]byte(strconv.Itoa(torrentID)), nil)
	return err
}

func (database DBLite) DeleteAllTorrents() error {
	batch := new(leveldb.Batch)
	iter := database.dbLevel.NewIterator(nil, nil)
	for iter.Next() {
		key := iter.Key()
		if string(key) == updateIDKey {
			continue
		}
		batch.Delete(key)
	}
	err := database.dbLevel.Write(batch, nil)
	return err
}

func (database DBLite) GetAllTorrents() ([]int, error) {
	var result []int
	iter := database.dbLevel.NewIterator(nil, nil)
	for iter.Next() {
		key := iter.Key()
		if string(key) == updateIDKey {
			continue
		}
		temp, err := getIntFromBytes(key)
		if err != nil {
			return result, err
		}
		result = append(result, temp)
	}
	return result, nil
}

func (database DBLite) GetChatByTorrentID(torrentID int) (int, error) {
	chatID, err := database.dbLevel.Get([]byte(strconv.Itoa(torrentID)), nil)
	if err != nil {
		return 0, err
	}
	return getIntFromBytes(chatID)
}

func (database DBLite) Close() {
	database.dbLevel.Close()
}

func New(name string) (*DBLite, error) {
	dbLevel, err := leveldb.OpenFile(name, nil)
	if err != nil {
		return &DBLite{}, err
	}
	hasUpdateID, err := dbLevel.Has([]byte(updateIDKey), nil)
	if err != nil {
		return &DBLite{dbLevel}, err
	}
	if !hasUpdateID {
		dbLevel.Put([]byte(updateIDKey), []byte("1"), nil)
	}
	return &DBLite{dbLevel}, nil
}
