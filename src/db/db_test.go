package db

import (
	"fmt"
	"os"
	"testing"

	"github.com/stretchr/testify/require"
)

const testDBNameTemplate = "torrent-test-%s.leveldb"

func setUpTest(dbname string) (*DBLite, error) {
	dbase, err := New(dbname)
	return dbase, err
}

func tearDownTest(dbname string) error {
	return os.RemoveAll(dbname)
}

func TestDBLite_AddTorrent(t *testing.T) {
	dbname := fmt.Sprintf(testDBNameTemplate, "add")
	dbase, err := setUpTest(dbname)
	require.Nil(t, err)

	err = dbase.AddTorrent(5, 1)
	require.Nil(t, err)
	result, err := dbase.dbLevel.Get([]byte(fmt.Sprintf("%d", 1)), nil)
	require.Nil(t, err)

	err = dbase.dbLevel.Close()
	require.Nil(t, err)
	require.Equal(t, fmt.Sprintf("%d", 5), string(result))
	err = tearDownTest(dbname)
	require.Nil(t, err)
}

func TestDBLite_DeleteAllTorrents(t *testing.T) {
	dbname := fmt.Sprintf(testDBNameTemplate, "delete-all")
	dbase, err := setUpTest(dbname)
	require.Nil(t, err)

	err = dbase.AddTorrent(1, 1)
	require.Nil(t, err)
	err = dbase.AddTorrent(2, 2)
	require.Nil(t, err)
	err = dbase.DeleteAllTorrents()
	require.Nil(t, err)
	res, err := dbase.dbLevel.Has([]byte(fmt.Sprintf("%d", 1)), nil)
	require.Nil(t, err)
	require.False(t, res)
	res, err = dbase.dbLevel.Has([]byte(fmt.Sprintf("%d", 1)), nil)
	require.Nil(t, err)
	require.False(t, res)
	res, err = dbase.dbLevel.Has([]byte(updateIDKey), nil)
	require.Nil(t, err)
	require.True(t, res)

	err = dbase.dbLevel.Close()
	require.Nil(t, err)
	err = tearDownTest(dbname)
	require.Nil(t, err)
}

func TestDBLite_DeleteTorrent(t *testing.T) {
	dbname := fmt.Sprintf(testDBNameTemplate, "delete")
	dbase, err := setUpTest(dbname)
	require.Nil(t, err)

	err = dbase.AddTorrent(2, 25)
	require.Nil(t, err)
	err = dbase.AddTorrent(12, 123)
	require.Nil(t, err)
	err = dbase.DeleteTorrent(123)
	require.Nil(t, err)
	res, err := dbase.dbLevel.Has([]byte(fmt.Sprintf("%d", 25)), nil)
	require.Nil(t, err)
	require.True(t, res)
	res, err = dbase.dbLevel.Has([]byte(fmt.Sprintf("%d", 123)), nil)
	require.Nil(t, err)
	require.False(t, res)
	res, err = dbase.dbLevel.Has([]byte(updateIDKey), nil)
	require.Nil(t, err)
	require.True(t, res)

	err = dbase.dbLevel.Close()
	require.Nil(t, err)
	err = tearDownTest(dbname)
	require.Nil(t, err)
}

func TestDBLite_GetAllTorrents(t *testing.T) {
	dbname := fmt.Sprintf(testDBNameTemplate, "get-all")
	dbase, err := setUpTest(dbname)
	require.Nil(t, err)

	torrents, err := dbase.GetAllTorrents()
	require.Nil(t, err)
	require.Equal(t, 0, len(torrents))
	err = dbase.AddTorrent(14, 12)
	require.Nil(t, err)
	err = dbase.AddTorrent(15, 1231)
	require.Nil(t, err)
	torrents, err = dbase.GetAllTorrents()
	require.Nil(t, err)
	require.Contains(t, torrents, 12)
	require.Contains(t, torrents, 1231)
	require.Equal(t, 2, len(torrents))

	err = dbase.dbLevel.Close()
	require.Nil(t, err)
	err = tearDownTest(dbname)
	require.Nil(t, err)
}

func TestDBLite_GetChatByTorrentID(t *testing.T) {
	dbname := fmt.Sprintf(testDBNameTemplate, "get")
	dbase, err := setUpTest(dbname)
	require.Nil(t, err)

	err = dbase.AddTorrent(14, 12)
	require.Nil(t, err)
	chatID, err := dbase.GetChatByTorrentID(12)
	require.Nil(t, err)
	require.Equal(t, 14, chatID)
	chatID, err = dbase.GetChatByTorrentID(123)
	require.EqualError(t, err, "leveldb: not found")

	err = dbase.dbLevel.Close()
	require.Nil(t, err)
	err = tearDownTest(dbname)
	require.Nil(t, err)
}

func TestDBLite_GetLastUpdate(t *testing.T) {
	dbname := fmt.Sprintf(testDBNameTemplate, "get-update")
	dbase, err := setUpTest(dbname)
	require.Nil(t, err)

	updateID, err := dbase.GetLastUpdate()
	require.Nil(t, err)
	require.Equal(t, 1, updateID)

	err = dbase.dbLevel.Close()
	require.Nil(t, err)
	err = tearDownTest(dbname)
	require.Nil(t, err)
}

func TestDBLite_UpdateLastUpdateID(t *testing.T) {
	dbname := fmt.Sprintf(testDBNameTemplate, "update-update")
	dbase, err := setUpTest(dbname)
	require.Nil(t, err)

	err = dbase.UpdateLastUpdateID(150)
	require.Nil(t, err)
	updateID, err := dbase.GetLastUpdate()
	require.Nil(t, err)
	require.Equal(t, 150, updateID)

	err = dbase.dbLevel.Close()
	require.Nil(t, err)
	err = tearDownTest(dbname)
	require.Nil(t, err)
}

func TestDBLite_Close(t *testing.T) {
	dbname := fmt.Sprintf(testDBNameTemplate, "close")
	dbase, err := setUpTest(dbname)
	require.Nil(t, err)

	dbase.Close()
	_, err = dbase.GetLastUpdate()
	require.EqualError(t, err, "leveldb: closed")

	err = tearDownTest(dbname)
	require.Nil(t, err)
}
