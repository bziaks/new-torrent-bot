package utils

import (
	"fmt"
	"io/ioutil"
	"path/filepath"
	"strings"
)

func BytesCount(bytes int64) string {
	const unit = 1024
	if bytes < unit {
		return fmt.Sprintf("%d B", bytes)
	}
	div, exp := int64(unit), 0
	for n := bytes / unit; n >= unit; n /= unit {
		div *= unit
		exp++
	}
	return fmt.Sprintf("%.3f %cB", float64(bytes)/float64(div), "KMGTPE"[exp])
}

func ListDirectory(directoryPath string) (files []string, err error) {
	filesInfo, err := ioutil.ReadDir(directoryPath)
	for _, file := range filesInfo {
		files = append(files, file.Name())
	}
	return
}

func FilepathHasPrefix(file, prefix string) (match bool, err error) {
	//Original filepath.HasPrefix is deprecated, so let's write our own
	correctPrefix, err := filepath.Abs(prefix)
	if err != nil {
		return
	}
	correctFile, err := filepath.Abs(file)
	if err != nil {
		return
	}
	match = strings.HasPrefix(filepath.FromSlash(correctFile), filepath.FromSlash(correctPrefix))
	return
}

type SimpleStringQueue struct {
	// Simple string queue, NOT THREADSAFE
	buf      []string
	capacity int
	length   int
}

func NewSimpleQueue(capacity int) *SimpleStringQueue {
	return &SimpleStringQueue{buf: make([]string, capacity), capacity: capacity, length: 0}
}

func (ssq *SimpleStringQueue) Put(s string) {
	if ssq.length < ssq.capacity {
		ssq.buf[ssq.length] = s
		ssq.length++
		return
	}
	for i := 1; i < ssq.length; i++ {
		ssq.buf[i-1] = ssq.buf[i]
	}
	ssq.buf[ssq.length-1] = s
}

func (ssq *SimpleStringQueue) Pop() (s string, err error) {
	if ssq.length == 0 {
		err = fmt.Errorf("queue is empty")
	}
	s = ssq.buf[0]
	for i := 1; i < ssq.length; i++ {
		ssq.buf[i-1] = ssq.buf[i]
	}
	ssq.length--
	return
}

func (ssq SimpleStringQueue) AsSlice() (s []string) {
	s = make([]string, ssq.length)
	for i := 0; i < ssq.length; i++ {
		s[i] = ssq.buf[i]
	}
	return
}
