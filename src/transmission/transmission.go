package transmission

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"sync"
	"time"
)

const (
	torrentGetMethod    = "torrent-get"
	torrentAddMethod    = "torrent-add"
	torrentRemoveMethod = "torrent-remove"
	torrentStopMethod   = "torrent-stop"
	torrentStartMethod  = "torrent-start"

	transmissionSessionIDHeader = "X-Transmission-Session-Id"
)

type RequestData struct {
	Method    string `json:"method"`
	Arguments struct {
		Fields   []string `json:"fields"`
		Ids      []int    `json:"ids,omitempty"`
		Metainfo []byte   `json:"metainfo"`
	} `json:"arguments"`
}

type Client struct {
	sessionID       string
	TransmissionURL string
	httpClient      *http.Client
	mx              sync.RWMutex
}

type Torrent struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

type TorrentStatus struct {
	ID           int     `json:"id"`
	Name         string  `json:"name"`
	TotalSize    int64   `json:"totalSize"`
	Status       int     `json:"status"`
	PercentDone  float64 `json:"percentDone"`
	RateDownload int64   `json:"rateDownload"`
}

type Response struct {
	Arguments struct {
		TorrentAdded     Torrent         `json:"torrent-added"`
		TorrentDuplicate Torrent         `json:"torrent-duplicate"`
		Torrents         []TorrentStatus `json:"torrents"`
	} `json:"arguments"`
	Result string `json:"result"`
}

func New(url string) *Client {
	tc := Client{}
	tc.TransmissionURL = url
	tr := &http.Transport{
		Proxy:           nil,
		MaxIdleConns:    10,
		IdleConnTimeout: 30 * time.Second,
	}
	client := &http.Client{Timeout: 25 * time.Second, Transport: tr}
	tc.httpClient = client
	resp, err := client.Get(tc.TransmissionURL)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
	tc.sessionID = resp.Header.Get(transmissionSessionIDHeader)
	return &tc
}

func (tc *Client) csrfAuth() error {
	tc.mx.Lock()
	defer tc.mx.Unlock()
	resp, err := tc.httpClient.Get(tc.TransmissionURL)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	tc.sessionID = resp.Header.Get(transmissionSessionIDHeader)
	return nil
}

func (tc *Client) setSessionID(sessionID string) {
	tc.mx.Lock()
	defer tc.mx.Unlock()
	tc.sessionID = sessionID
}

func (tc *Client) getSessionID() string {
	tc.mx.RLock()
	defer tc.mx.RUnlock()
	return tc.sessionID
}

func (tc *Client) makeRequest(data RequestData) (string, error) {
	body, err := json.Marshal(data)
	if err != nil {
		return "", err
	}
	req, err := http.NewRequest(http.MethodPost, tc.TransmissionURL, bytes.NewBuffer(body))
	if err != nil {
		return "", err
	}
	req.Header.Set(transmissionSessionIDHeader, tc.getSessionID())
	req.Header.Set("Content-Type", "application/json")
	resp, err := tc.httpClient.Do(req)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()
	if resp.StatusCode == 200 {
		respBody, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return "", err
		}
		return string(respBody), nil
	}
	err = tc.csrfAuth()
	if err != nil {
		return "", err
	}
	req, err = http.NewRequest(http.MethodPost, tc.TransmissionURL, bytes.NewBuffer(body))
	if err != nil {
		return "", err
	}
	req.Header.Set(transmissionSessionIDHeader, tc.getSessionID())
	req.Header.Set("Content-Type", "application/json")
	respSecondTry, err := tc.httpClient.Do(req)
	if err != nil {
		return "", err
	}
	defer respSecondTry.Body.Close()
	if respSecondTry.StatusCode == 200 {
		respBody, err := ioutil.ReadAll(respSecondTry.Body)
		if err != nil {
			return "", err
		}
		return string(respBody), nil
	}

	return "", nil
}

func (tc *Client) GetStatusAll() (Response, error) {
	data := RequestData{Method: torrentGetMethod}
	data.Arguments.Fields = []string{"id", "name", "totalSize", "status", "percentDone", "rateDownload"}
	resp, err := tc.makeRequest(data)
	if err != nil {
		return Response{}, err
	}
	var result Response
	err = json.Unmarshal([]byte(resp), &result)
	if err != nil {
		return Response{}, err
	}
	return result, nil
}

func (tc *Client) GetTorrent(id int) (Response, error) {
	data := RequestData{Method: torrentGetMethod}
	data.Arguments.Fields = []string{"id", "name", "totalSize", "status", "percentDone", "rateDownload"}
	data.Arguments.Ids = []int{id}
	resp, err := tc.makeRequest(data)
	if err != nil {
		return Response{}, err
	}
	var result Response
	err = json.Unmarshal([]byte(resp), &result)
	if err != nil {
		return Response{}, err
	}
	return result, nil
}

func (tc *Client) StopTorrent(id int) (Response, error) {
	data := RequestData{Method: torrentStopMethod}
	data.Arguments.Ids = []int{id}
	resp, err := tc.makeRequest(data)
	if err != nil {
		return Response{}, err
	}
	var result Response
	err = json.Unmarshal([]byte(resp), &result)
	if err != nil {
		return Response{}, err
	}
	return result, nil
}

func (tc *Client) StartTorrent(id int) (Response, error) {
	data := RequestData{Method: torrentStartMethod}
	data.Arguments.Ids = []int{id}
	resp, err := tc.makeRequest(data)
	if err != nil {
		return Response{}, err
	}
	var result Response
	err = json.Unmarshal([]byte(resp), &result)
	if err != nil {
		return Response{}, err
	}
	return result, nil
}

func (tc *Client) SetTorrentToDownload(info []byte) (Response, error) {
	data := RequestData{Method: torrentAddMethod}
	data.Arguments.Metainfo = info
	resp, err := tc.makeRequest(data)
	if err != nil {
		return Response{}, err
	}
	var result Response
	err = json.Unmarshal([]byte(resp), &result)
	if err != nil {
		return Response{}, err
	}
	if result.Result != "success" {
		return Response{}, err
	}
	return result, nil
}

func (tc *Client) RemoveTorrent(ids []int) (string, error) {
	data := RequestData{Method: torrentRemoveMethod}
	data.Arguments.Ids = ids
	return tc.makeRequest(data)
}
