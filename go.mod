module gitlab.com/bziaks/new-torrent-bot

go 1.13

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/mattn/go-sqlite3 v2.0.3+incompatible
	github.com/pkg/errors v0.8.1
	github.com/stretchr/testify v1.4.0
	github.com/syndtr/goleveldb v1.0.0
	golang.org/x/net v0.0.0-20191209160850-c0dbc17a3553 // indirect
	golang.org/x/sync v0.0.0-20190423024810-112230192c58 // indirect
	golang.org/x/sys v0.0.0-20200117145432-59e60aa80a0c
	gopkg.in/yaml.v2 v2.2.2
)
